package ru.codemika;

import java.io.IOException;

public class Main {
    private static CredentialsProperties creds;

    public static void main(String[] args) {
        config();

        var bot = new Instalike(
                creds.getUsername(),
                creds.getPassword(),
                "codemika"
        );
        bot.start();
    }

    private static void config() {
        System.setProperty(
                "java.util.logging.SimpleFormatter.format",
                "%1$tF %1$tT [%4$s]: %5$s%6$s%n"
        );

//        System.setProperty(
//                "webdriver.chrome.driver",
//                "D:\\Downloads\\chromedriver.exe"
//        );

        try {
            creds = new CredentialsProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
