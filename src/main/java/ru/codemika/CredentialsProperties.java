package ru.codemika;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Вытаскиваем имя пользователя и пароль из файла конфигурации.
 */
class CredentialsProperties {

    private final static String propsFileName = "credentials.properties";

    private String username;
    private String password;

    CredentialsProperties() throws IOException {
        InputStream inputStream = null;
        try {
            Properties props = new Properties();
            inputStream = getClass().getClassLoader().getResourceAsStream(propsFileName);
            if (inputStream != null) {
                props.load(inputStream);
            } else {
                throw new FileNotFoundException(String.format("Property file '%s' not found in the classpath", propsFileName));
            }

            username = props.getProperty("username");
            password = props.getProperty("password");
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
