package ru.codemika;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Map.entry;

/**
 * Instalike.
 * Для работы требуется Google Chrome и Selenium с драйвером.
 */
class Instalike {

    private final Map<String, String> xpaths = Map.ofEntries(
            entry("login_button", "//*[@id=\"react-root\"]/section/main/div/article/div/div[1]/div/form/div[3]/button"),
            entry("first_post", "//*[@id=\"react-root\"]/section/main/div/div[3]/article/div/div/div[1]/div[1]"),
            entry("like_button", "/html/body/div[3]/div/div[2]/div/article/div[2]/section[1]/span[1]/button"),
            entry("next_button_first", "/html/body/div[3]/div/div[1]/div/div/a"),
            entry("next_button_default", "/html/body/div[3]/div/div[1]/div/div/a[2]"),
            entry("next_button_last", "/html/body/div[3]/div/div[1]/div/div/a")
    );

    private final String username;
    private final String password;
    private final String target;
    private final int baseDelay;
    private Logger logger;
    private WebDriver browser;

    Instalike(String username, String password, String target) {
        this(username, password, target, 3);
    }

    Instalike(String username, String password, String target, int baseDelay) {
        this.username = username;
        this.password = password;
        this.target = target;
        this.baseDelay = baseDelay;
        logger = Logger.getLogger(Instalike.class.getName());
    }

    void start() {
        log(String.format("App started with target '%s'", target));
        browser = new ChromeDriver();
        browser.manage().timeouts().implicitlyWait(baseDelay, TimeUnit.SECONDS);
        login();
        if (findFirst()) {
            likeCurrent();
            while (getNext()) {
                likeCurrent();
            }
        }
        end();
    }

    private void login() {
        browser.get("https://www.instagram.com/accounts/login/");
        waitASec();
        browser.findElement(new By.ByName("username")).sendKeys(username);
        browser.findElement(new By.ByName("password")).sendKeys(password);
        browser.findElement(new By.ByXPath(xpaths.get("login_button"))).click();
        // Enter SMS code here.
        waitASec();
        log(String.format("Logged in as %s", username));
    }

    private boolean findFirst() {
        browser.get(String.format("https://instagram.com/%s", target));
        waitASec();
        log(String.format("Working on %s", browser.getTitle()));
        try {
            browser.findElement(new By.ByXPath(xpaths.get("first_post"))).click();
            waitASec();
            return true;
        }
        catch (NoSuchElementException e) {
            warn(String.format("Can't find first photo of %s. May be profile is empty or closed.", target));
            return false;
        }
    }

    private void likeCurrent() {
        var likeButton = browser.findElement(new By.ByXPath(xpaths.get("like_button")));
        try {
            browser.findElement(new By.ByClassName("glyphsSpriteHeart__filled__24__red_5"));
            log("Already liked!");
        }
        catch (NoSuchElementException e) {
            likeButton.click();
            log("Liked!");
            waitASec();
        }
    }

    private boolean getNext() {
        try {
            browser.findElement(new By.ByClassName("coreSpriteRightPaginationArrow")).click();
            waitASec();
            return true;
        }
        catch (NoSuchElementException e) {
            log("Can't get next photo. It's supposed to be done.");
            return false;
        }
    }

    private void end() {
        browser.close();
        log("Nox!");
    }

    private void waitASec() {
        try {
            Thread.sleep((baseDelay + ThreadLocalRandom.current().nextInt(1, 3)) * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void log(String message) {
        logger.log(Level.INFO, String.format("Instalike: %s", message));
    }

    private void warn(String message) {
        logger.log(Level.WARNING, String.format("Instalike: %s", message));
    }
}
